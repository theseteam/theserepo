%########################################################################
% 
% Filename : sattarov-thesis.cls
% Author   : Vincent Feuvrier
% Contact  : vincent.feuvrier@normalesup.org
%
% Simple to use LaTeX class for writing thesis in mathematics at 
% Paris-Sud university.
%
% See included manual files for usage.
%
%########################################################################

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{sattarov-thesis}[2008/10/10 Custom class for writing thesis in mathematics at Paris-Sud University]

%########################################################################
% Options
%########################################################################

\RequirePackage{ifthen}
\RequirePackage{keyval}

\RequirePackage{kvoptions}

\DeclareOption{hyperref}


\ProcessOptions\relax
\ProcessKeyvalOptions{sattarov-thesis}%

%########################################################################
% Base class and packages
%########################################################################

\LoadClass[a4paper,twoside,12pt]{report}%{article}

%Language
\RequirePackage[english,french]{babel}
\RequirePackage{textcomp}
\RequirePackage[autolanguage]{numprint}

%Page setup
\RequirePackage[hmargin={3cm,2.3cm},vmargin={3cm,2cm}]{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}

\RequirePackage{mmap}
%Logos

\usepackage{graphicx}
\RequirePackage{ifpdf}

%Math
\RequirePackage{amsmath,amssymb,amsfonts}

%Misc
\RequirePackage{tabularx}
\usepackage{framed}
\usepackage{multicol}

\usepackage{gensymb}
\usepackage{subcaption} 
\graphicspath{{images/}{logos/}}
\usepackage[utf8]{inputenc}


%\usepackage{glossaries}
\usepackage{textpos}
\usepackage{listings}
%\usepackage{cite}
\usepackage{url}

\usepackage{etoc}
%\usepackage[square,sort,comma,numbers,authoryear]{natbib}

\usepackage[maxbibnames=10,backref=true,backend=biber,citestyle=authoryear,style=authoryear]{biblatex}


\usepackage{bibentry}

%\usepackage[pagebackref]{hyperref}
\usepackage{hyperref}
\usepackage{xcolor}
%\nobibliography*

%\usepackage{chapterbib}
\usepackage{dirtree}
\usepackage{setspace}
%\usepackage{algorithm}
%\usepackage{algpseudocode}
\usepackage[ruled,noend,noline,slide]{algorithm2e}

%\usepackage{placeins} 
%\makeglossaries
%########################################################################
% Page setup
%########################################################################
\newcommand{\footone}{
    \begin{tabular}{l}%
    \textbf{Université Paris-Saclay}\\%
    Espace Technologique / Immeuble Discovery \\
    Route de l'Orme aux Merisiers RD \numprint{128} / 91190 Saint-Aubin, France%
    \end{tabular}%
    \hfill%
    \includelogo[height=0.9in]{logoEgrey}
}%


\tolerance=10000
\hbadness=10000

\pagestyle{fancy}{
\fancyhf{}
\fancyhead[RO,LE]{\footnotesize\textbf{\thepage}}
\fancyhead[RE]{\footnotesize\scshape\rightmark}
\fancyhead[LO]{\footnotesize\leftmark}
}
\fancypagestyle{abstract}{
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyfoot[L]{\footnotesize\footone}
}

\setlength\headheight{16pt}
\setlength\headsep{\baselineskip}

%########################################################################
% Title page
%########################################################################



\newcommand\ordernumber[1]{%
  \gdef\@ordernumber{#1}%
}

\newcounter{@member}

\newcommand\addcommissionmember[4][]{%
  \stepcounter{@member}%
  \ifthenelse{\equal{#1}{}}{%
    \expandafter\def\csname @member@\the@member\endcsname{#2&#3&#4&}%
  }{%
    \expandafter\def\csname @member@\the@member\endcsname{#1&#2&#3&#4}%
  }%
}


\newcommand\includelogo[2][]{%
  \ifpdf%
    \includegraphics[#1]{logos/#2}%
  \else%
    \includegraphics[#1]{logos/#2.eps}%
  \fi%
}

\renewcommand\maketitle{%
  \selectlanguage{frenchb}%
  \setcounter{page}{1}%
  \thispagestyle{empty}%
  \newgeometry{top=0.591in,left=0.788in,right=0.788in,bottom=0.492in}
  \noindent%
  \includelogo[height=0.8in]{university.png}%
  \hfill%
  \includelogo[height=0.8in]{sud_saclay.jpg}%
  {\normalsize\\}%
  \begin{framed}
  {\fontsize{10}{\baselineskip}\textbf{ NNT: \@ordernumber}}%
  \\[\bigskipamount]%
  \begin{center}%
    \fontsize{18}{18}
    \textbf{\textsc{Thèse de Doctorat} \\ 
    \textsc{de} \\
    \textsc{l'Université Paris-Saclay}\\
    \textsc{préparée à} \\
    \textsc{l'Université Paris-Sud XI}}%
    \\[1.5\bigskipamount]%
    \begin{minipage}{0.65\textwidth}%
      %\large%
      \fontsize{14}{\baselineskip}
      \centering%
      	\textsc{Ecole Doctorale \textnumero 580}\\%
    \end{minipage}%
    \\[\smallskipamount]%
    %\large%
    Sciences et technologies de l'information et de la communication%
    \\[\medskipamount]%
    Spécialité de doctorat: Traitement du Signal et des images
    \vspace{0.2 in}
    \\[\bigskipamount]%
    \fontsize{11}{\baselineskip}
    par
    \\[\medskipamount]%
    \fontsize{16}{\baselineskip}
    \textbf{M. \@author}%
    \\[4\bigskipamount]%
    \begin{minipage}{0.9\textwidth}%
      %\large
      \fontsize{14}{\baselineskip}
      %\bfseries%
      \centering%
      \titlefr
      \selectlanguage{frenchb}%
    \end{minipage}%
  \end{center}%
  \vfill%
  \vspace{1 in}
  \ifnum\the@member=0\relax%
    \ClassWarning{sattarov-thesis}{No commission member have been given}{You can provide members using the command \string\addcommissionmember.}%
  \else%
    \normalsize%
    \textbf{Thèse présentée et soutenue à Gif-sur-Yvette, le 9 décembre 2016}
    \\[\smallskipamount]
    \raggedright
    \textbf{Composition du Jury:}\\
    \@ifundefined{c@@tempa}{\newcounter{@tempa}}{}%
    \setcounter{@tempa}{0}%
    \fontsize{11}{\baselineskip}
    \begin{tabular}{l l l l}%
      
      \multicolumn{4}{l}{}\\[\medskipamount]%
      \whiledo{\value{@tempa}<\value{@member}}
      {%
        \stepcounter{@tempa}%
        \csname @member@\the@tempa\endcsname%
        \ifnum\value{@tempa}<\value{@member}\relax%
          \\%
        \fi%
      }%
    
    \end{tabular}
  \fi%
  \vfill
  
  \end{framed}

  \small%
  \selectlanguage{english}
  \normalsize%
}


\makeatletter

\newrobustcmd*{\parentexttrack}[1]{%
  \begingroup
  \blx@blxinit
  \blx@setsfcodes
  \blx@bibopenparen#1\blx@bibcloseparen
  \endgroup}

\AtEveryCite{%
  \let\parentext=\parentexttrack%
  \let\bibopenparen=\bibopenbracket%
  \let\bibcloseparen=\bibclosebracket}

\makeatother


\makeatletter
%\renewcommand{\p@section}{\thepart.} % prefix "\thepart." to the number of the chapter
\renewcommand{\p@section}{\thechapter.}
\makeatother

\renewcommand\cite{\parencite}
\newcommand{\lidar}{LIDAR}
\newcommand{\Matrix}[1] {\bold{#1}}
\newcommand{\Vector}[1] {\bold{#1}}
\newcommand{\Space}[1]{\mathcal{#1}}
\newcommand{\Vecind}[1]{_{#1}}
\newcommand{\Matrind}[2]{_{#1#2}}
\newcommand{\Timeind}[1]{(#1)}
\newcommand{\Norm}[1]{\| #1 \|}
\newcommand{\Filename}[1]{\texttt{#1}}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}
%\newcommand{\Definition}[1] {\paragraph{#1}}
\newcommand{\Definition}[1] {\textbf{#1}}
%\newcommand{\norm}[1]{\mathfrak{#1}}
%\newcommand{\norm}[1]{\mathfrak{#1}}
\renewcommand*\DTstylecomment{\rmfamily\textsc}
%\def\thepart{\arabic{part}}
%\def\thesection{\thepart.\arabic{section}}
%\def\thesubsection{\thesection.\arabic{subsection}}
%\def\thesubsubsection{\thesubsection.\arabic{subsubsection}}

%\renewcommand{\l@section}{\bprot@dottedtocline{1}{2em}{2.7em}}
%\renewcommand{\l@subsection}{\bprot@dottedtocline{2}{3.8em}%{3.2em}}
%\renewcommand{\l@subsubsection}{\bprot@dottedtocline{3}{7.0em}{4.1em}}

\makeatletter
%\@addtoreset{section}{part}
\@addtoreset{section}{chapter}
\makeatother 
%\@addtoreset{section}{part}